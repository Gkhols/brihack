class EztapsController < ApplicationController
  before_action :set_eztap, only: [:update, :destroy]

  # GET /eztaps
  def index
    @eztaps = Eztap.all

    render json: {data: @eztaps}, :include => :riwayatregistrasis
  end

  # GET /eztaps/1
  def show
    @riwayatregistrasi = Riwayatregistrasi.all
    render json: @riwayatregistrasi
  end

  # POST /eztaps
  def create
    #NLP
    token = "Bearer d61484e2-6308-4b74-b5bb-d8918e977706"
    response = HTTParty.post(
      "https://geist.kata.ai/nlus/hackathoniuse:bri_hack/predict",
      headers: {
        Authorization: token
      },
      body: {
        text: params[:text].to_s
      }
    ).to_s

    respjson = JSON.parse(response)

    jnama = respjson['result']['nama'][0]['value'].to_s
    jalamat = respjson['result']['alamat'][0]['value'].to_s
    jnohp = respjson['result']['nohp'][0]['value'].to_s
    jorder = respjson['result']['order'][0]['value'].to_s
    jpembayaran = respjson['result']['pembayaran'][0]['resolved']['dictKey'].to_s

    @payload = jnama + jalamat + jnohp + jorder + jpembayaran
    print @payload


    @newparams = eztap_params
    @newparams[:nama] = jnama
    @newparams[:alamat] = jalamat
    @newparams[:nohp] = jnohp
    @newparams[:order] = jorder
    @newparams[:pembayaran] = jnama
    @newparams[:noresi] = Digest::SHA512.hexdigest(Time.now.to_s)[0..8]
    @newparams[:harga] = "Rp 100.000"
    @newparams[:status] = "belum dibayar"

    @eztap = Eztap.new(@newparams)

    if @eztap.save
      try = Riwayatregistrasi.new(eztap_id: @eztap.id, riwayat: "belum dibayar")
      if try.save
        render json: @eztap, status: :created
      else 
        render json: {error: try.errors}
      end
      
    else
      render json: @eztap.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /eztaps/1
  def update
    if @eztap.update(eztap_params)
      try = Riwayatregistrasi.new(eztap_id: @eztap.id, riwayat: params[:status].to_s)
      if try.save
      render json: @eztap
      else 
        render json: {error: try.errors}
      end
    else
      render json: @eztap.errors, status: :unprocessable_entity
    end
  end

  # DELETE /eztaps/1
  def destroy
    @eztap.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eztap
      @eztap = Eztap.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def eztap_params
      params.permit(:nama, :alamat, :nohp, :order, :pembayaran, :noresi, :eztap_id, :harga, :status)
    end
end
