class AuthController < ApplicationController

  def account_info
    #token
    @request = HTTParty.post(
      "https://sandbox.partner.api.bri.co.id/oauth/client_credential/accesstoken?grant_type=client_credentials",
      headers: {
        'Content-Type' => 'application/x-www-form-urlencoded'
      },
      body: {
        client_id: "6MyKTHdfDyUs1sxHMrFMb4bf3ygq7VLZ",
        client_secret: "pKGHi9zcEPxplY7e"
      }).to_s
      parsing = JSON.parse(@request)
      access_token = parsing['access_token'].to_s
      @token = "Bearer "+access_token

      #signature 
      digest = OpenSSL::Digest.new('sha256')
      @key = "pKGHi9zcEPxplY7e"
      @time = Time.zone.now.strftime('%Y-%m-%dT%H:%M:%S.%LZ').to_s
      @payload = "path=/sandbox/v2/inquiry/888801000157508&verb=GET&token="+@token+"&timestamp="+@time+"&body="
      @hmac = Base64.encode64(OpenSSL::HMAC.digest(digest, @key, @payload)).to_s
      @response = HTTParty.get("https://partner.api.bri.co.id/sandbox/v2/inquiry/888801000157508",
      headers: {
        'Authorization' => @token,
        'BRI-Signature' => @hmac,
        'BRI-Timestamp' => @time
      }).to_s
      @respjson = JSON.parse(@response)
      print @payload + @hmac
      render json: {bri: parsing, signature: @hmac, inquiry: @respjson}
  end

  def mutation_info
    #token
    @request = HTTParty.post(
      "https://sandbox.partner.api.bri.co.id/oauth/client_credential/accesstoken?grant_type=client_credentials",
      headers: {
        'Content-Type' => 'application/x-www-form-urlencoded'
      },
      body: {
        client_id: "6MyKTHdfDyUs1sxHMrFMb4bf3ygq7VLZ",
        client_secret: "pKGHi9zcEPxplY7e"
      }).to_s
      parsing = JSON.parse(@request)
      access_token = parsing['access_token'].to_s
      @token = "Bearer "+access_token

      #signature 
      digest = OpenSSL::Digest.new('sha256')
      @key = "pKGHi9zcEPxplY7e"
      datenow = Date.current
      dateago = datenow - 30.day
      @time = Time.zone.now.strftime('%Y-%m-%dT%H:%M:%S.%LZ').to_s
      @payload = "path=/v1/statement/888801000157508/"+datenow.to_s+"/"+dateago.to_s+"&verb=GET&token="+@token+"&timestamp="+@time+"&body="
      @hmac = Base64.encode64(OpenSSL::HMAC.digest(digest, @key, @payload)).to_s
      @response = HTTParty.get("https://sandbox.partner.api.bri.co.id/v1/statement/888801000157508/"+datenow.to_s+"/"+dateago.to_s,
      headers: {
        'Authorization' => @token,
        'BRI-Signature' => @hmac,
        'BRI-Timestamp' => @time
      }).to_s
      @respjson = JSON.parse(@response)
      print @payload + @hmac
      render json: {bri: parsing, signature: @hmac, mutation: @respjson}
    end
end
