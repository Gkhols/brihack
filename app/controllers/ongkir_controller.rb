class OngkirController < ApplicationController
  def index_province
    url = "https://api.rajaongkir.com/starter/province"
    key = "ad9053492d6cea72947b14e905927071"
    response = HTTParty.get(url, 
    headers: {
      'key' => 'ad9053492d6cea72947b14e905927071'
    }).to_s
    @resp = JSON.parse(response)
    render json: @resp
  end

  def index_city
    url = "https://api.rajaongkir.com/starter/city"
    key = "ad9053492d6cea72947b14e905927071"
    response = HTTParty.get(url, 
    headers: {
      'key' => 'ad9053492d6cea72947b14e905927071'
    }).to_s
    @resp = JSON.parse(response)
    render json: @resp
  end

  def cost_ongkir 
    url = "https://api.rajaongkir.com/starter/cost"
    key = "ad9053492d6cea72947b14e905927071"
    origin = params[:asal].to_s
    destination = params[:tujuan].to_s
    weight = params[:berat]
    courier = params[:kurir].to_s
    request = HTTParty.post(
      "https://api.rajaongkir.com/starter/cost",
      headers: {
        'Content-Type' => 'application/x-www-form-urlencoded',
        'key' => 'ad9053492d6cea72947b14e905927071'
      },
      body: {
        origin: origin,
        destination: destination,
        weight: weight,
        courier: courier
      })
      
      render json: {hasil: request}
  end

end
