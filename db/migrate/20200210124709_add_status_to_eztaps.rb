class AddStatusToEztaps < ActiveRecord::Migration[5.2]
  def change
    add_column :eztaps, :status, :string
  end
end
