class CreateRiwayatregistrasis < ActiveRecord::Migration[5.2]
  def change
    create_table :riwayatregistrasis do |t|
      t.references :eztap, foreign_key: true
      t.string :riwayat

      t.timestamps
    end
  end
end
