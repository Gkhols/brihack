class CreateEztaps < ActiveRecord::Migration[5.2]
  def change
    create_table :eztaps do |t|
      t.string :nama
      t.text :alamat
      t.string :nohp
      t.string :order
      t.string :pembayaran
      t.string :noresi

      t.timestamps
    end
  end
end
