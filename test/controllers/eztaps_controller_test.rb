require 'test_helper'

class EztapsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @eztap = eztaps(:one)
  end

  test "should get index" do
    get eztaps_url, as: :json
    assert_response :success
  end

  test "should create eztap" do
    assert_difference('Eztap.count') do
      post eztaps_url, params: { eztap: { alamat: @eztap.alamat, nama: @eztap.nama, nohp: @eztap.nohp, noresi: @eztap.noresi, order: @eztap.order, pembayaran: @eztap.pembayaran } }, as: :json
    end

    assert_response 201
  end

  test "should show eztap" do
    get eztap_url(@eztap), as: :json
    assert_response :success
  end

  test "should update eztap" do
    patch eztap_url(@eztap), params: { eztap: { alamat: @eztap.alamat, nama: @eztap.nama, nohp: @eztap.nohp, noresi: @eztap.noresi, order: @eztap.order, pembayaran: @eztap.pembayaran } }, as: :json
    assert_response 200
  end

  test "should destroy eztap" do
    assert_difference('Eztap.count', -1) do
      delete eztap_url(@eztap), as: :json
    end

    assert_response 204
  end
end
