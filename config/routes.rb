Rails.application.routes.draw do
  get 'ongkir/index/province' => 'ongkir#index_province'
  get 'ongkir/index/city' => 'ongkir#index_city'
  post 'ongkir/cost/calculation' => 'ongkir#cost_ongkir'
  post 'eztap/create/invoice' => 'eztaps#create'
  get 'eztap/update/:id/:status' => 'eztaps#update'
  get 'eztap/all/invoices' => 'eztaps#index'
  get 'auth/account_info' => 'auth#account_info'
  get 'auth/mutation_info' => 'auth#mutation_info'
  get 'riwayatreg' => 'eztaps#show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
